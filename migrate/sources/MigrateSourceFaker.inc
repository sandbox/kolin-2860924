<?php

/**
 * @file
 * Define a MigrateSource, to generate random items using Faker.
 */

/**
 * Implementation of MigrateSource, to generate random items using Faker.
 */
class MigrateSourceFaker extends MigrateSource {
  /**
   * List of available source fields.
   *
   * @var array
   */
  protected $fields = array();

  /**
   * The faker object.
   *
   * @var object
   */
  protected $faker;

  /**
   * The amount of items to generate.
   *
   * @var int
   */
  protected $count;

  /**
   * The fields with faker definitions.
   *
   * @var array
   */
  protected $fakerFields;

  /**
   * The current row id.
   *
   * @var int
   */
  protected $currentRowID;

  /**
   * Simple initialization.
   *
   * @param int $count
   *   The amount of items to import.
   * @param array $fields
   *   The fields with options for each.
   * @param string $locale
   *   The locale to use for faker.
   */
  public function __construct($count, array $fields, $locale = NULL) {
    libraries_load('Faker');
    $this->currentRowID = 0;
    $this->faker = Faker\Factory::create($locale);
    $this->count = $count;
    $this->fakerFields = $fields;
    $this->fields = array('id' => '');
    $this->fields = array_merge($this->fields, $fields);
  }

  /**
   * Return a string representing the source query.
   *
   * @return string
   *   a string representing the source query.
   */
  public function __toString() {
    return print_r($this->fakerFields, TRUE);
  }

  /**
   * Returns a list of fields available to be mapped from the source query.
   *
   * @return array
   *   Keys: machine names of the fields (to be passed to addFieldMapping)
   *   Values: Human-friendly descriptions of the fields.
   */
  public function fields() {
    foreach ($this->fields as $k => $v) {
      $fields[$k] = ucwords(str_replace('_', ' ', $k));
    }
    return $fields;
  }

  /**
   * Return a count of all available source records.
   *
   * @param bool $refresh
   *   Refresh - We never use this because the count is always passed to the
   *   constructor.
   *
   * @return int
   *   The amount of records to create.
   */
  public function count($refresh = FALSE) {
    return $this->count;
  }

  /**
   * Implementation of MigrateSource::performRewind().
   */
  public function performRewind() {

  }

  /**
   * Generate the data for the current row.
   *
   * @return object
   *   The data for the current row.
   */
  protected function getNextRow() {
    // todo: Generate the $row array here.
    if ($this->currentRowID < $this->count()) {

      $row = array();
      $row['id'] = $this->currentRowID;

      foreach ($this->fakerFields as $key => $value) {
        if (is_array($value)) {
          $row[$key] = $this->faker->$value['format']($value['options']);
        }
        else {
          $row[$key] = $this->faker->$value;
        }
        if (is_object($row[$key]) && is_a($row[$key], 'DateTime')) {
          $row[$key] = $row[$key]->getTimestamp();
        }
      }

      $this->currentRowID++;
      return (object) $row;
    }
    else {
      return NULL;
    }
  }

}
